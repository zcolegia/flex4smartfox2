package com.fsf2.components {

	import com.smartfoxserver.v2.SmartFox;
	import com.smartfoxserver.v2.core.SFSEvent;
	import com.smartfoxserver.v2.entities.Room;
	import com.smartfoxserver.v2.entities.SFSRoom;
	import com.smartfoxserver.v2.entities.managers.IRoomManager;
	
	import flash.events.Event;
	import flash.utils.getQualifiedClassName;
	
	import mx.binding.utils.BindingUtils;
	import mx.binding.utils.ChangeWatcher;
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.collections.IList;
	import mx.collections.IViewCursor;
	import mx.controls.Alert;
	import mx.events.FlexEvent;
	import mx.states.OverrideBase;
	import mx.utils.ObjectUtil;
	
	import spark.collections.Sort;
	import spark.collections.SortField;
	import spark.components.supportClasses.ListBase;
	import spark.components.supportClasses.SkinnableComponent;
	import spark.components.supportClasses.SkinnableContainerBase;

	[SkinState("notReady")]
	[SkinState("disconnected")]
	[SkinState("connected")]
	[SkinState("loggedIn")]

	public class SF2RoomList extends SkinnableComponent {

		public static const ROOM_TYPE_REG:String="Regular";
		public static const ROOM_TYPE_GAME:String="Game";
		public static const ROOM_TYPE_LIMBO:String="Limbo";

		[SkinPart(required="false")]
		public var roomListContainer:ListBase;

		[SkinPart(required="false")]
		public var roomGridContainer:SkinnableContainerBase;

		private var _smartFox:SmartFox;

		public function get smartFox():SmartFox {
			return _smartFox;
		}

		public function set smartFox(value:SmartFox):void {

			if (_smartFox === value)
				return;

			if (_smartFox) {
				removeEvents(_smartFox);
			}

			_smartFox=value;
			if (_smartFox) {
				addEvents(_smartFox);

			}

			generateRoomListData();
			invalidateSkinState();
		}


		private var _roomsListData:IList;

		public function set roomsListData(value:IList):void {
			if (_roomsListData !== value) {
				_roomsListData=value;
				dispatchEvent(new Event("roomsListDataChange"));
			}
		}


		[Bindable(event="roomsListDataChange")]
		public function get roomsListData():IList {
			return _roomsListData;
		}



		public function SF2RoomList() {
			super();
		}

		override protected function getCurrentSkinState():String {

			if (!this.smartFox) {
				return "notReady";
			} else {

				if (!this.smartFox.isConnected) {
					return "disconnected";
				} else {
					if (this.smartFox.mySelf) {
						return "loggedIn";
					} else {
						return "connected";
					}
				}
			}
		}

		override protected function partAdded(partName:String, instance:Object):void {
			super.partAdded(partName, instance);
			mytrace("part added " + instance);

			if (instance == roomListContainer) {
				var ch:ChangeWatcher=BindingUtils.bindProperty(roomListContainer, "dataProvider", this, ["roomsListData"]);
				if (!ch.isWatching()) {
					throw new Error("something is wrong binding did not work on roomListContainer");
				}
			} else if (instance == roomGridContainer) {
				var ch2:ChangeWatcher=BindingUtils.bindProperty(roomGridContainer, "dataProvider", this, ["roomsListData"]);
				if (!ch2.isWatching()) {
					throw new Error("something is wrong binding did not work on roomGridContainer");
				}
			}
		}

		override protected function childrenCreated():void {
			generateRoomListData();
			super.childrenCreated();
		}

		override protected function partRemoved(partName:String, instance:Object):void {
			super.partRemoved(partName, instance);

		}

		private var roomListRawData:Array;

		private function generateRoomListData():void {
			
			
			
			
			if (isLoggedIn()) {
				var rm:IRoomManager = smartFox.roomManager;
				
				
				trace("joined rooms "+rm.getJoinedRooms());
				trace("room count "+rm.getRoomCount());
				trace("group list [LobbyGroup] "+rm.getRoomListFromGroup("LobbyGroup"));
				trace("group list [Gbeirut] "+rm.getRoomListFromGroup("Gbeirut"));
				trace("room Groups "+rm.getRoomGroups());
				
				if (roomListRawData != smartFox.roomManager.getRoomList()) {
					roomListRawData=smartFox.roomManager.getRoomList();
					roomsListData=transformListItems(roomListRawData);

				}

			} else {
				roomsListData=null;
			}
		}

		private function addEvents(sf:SmartFox):void {

			mytrace("adding event listeners");

			sf.addEventListener(SFSEvent.CONNECTION, onConnectionListener);
			sf.addEventListener(SFSEvent.CONNECTION_LOST, onConnectionLostListener);
			sf.addEventListener(SFSEvent.CONNECTION_RETRY, onConnectionRetryListener);
			sf.addEventListener(SFSEvent.CONNECTION_RESUME, onConnectionResumeListener);

			sf.addEventListener(SFSEvent.LOGIN, onLoginListener);
			sf.addEventListener(SFSEvent.LOGOUT, onLogoutListener);

			sf.addEventListener(SFSEvent.USER_COUNT_CHANGE, onUserCountChangeListener);

			sf.addEventListener(SFSEvent.ROOM_JOIN, onRoomJoinListener);
			sf.addEventListener(SFSEvent.USER_EXIT_ROOM, onExitListener);
			sf.addEventListener(SFSEvent.ROOM_JOIN_ERROR, onRoomJoinErrorListener);
			sf.addEventListener(SFSEvent.ROOM_ADD, onRoomAddedListener);
			sf.addEventListener(SFSEvent.ROOM_REMOVE, onRoomRemoveListener);
			sf.addEventListener(SFSEvent.ROOM_NAME_CHANGE, onRoomNameChangeListener);
			sf.addEventListener(SFSEvent.ROOM_CAPACITY_CHANGE, onRoomCapacityChangedListener);
			sf.addEventListener(SFSEvent.ROOM_PASSWORD_STATE_CHANGE, onRoomPasswordStateChangeListener);
			
			
		}

		private function stateChangeListener(e:FlexEvent):void {

			generateRoomListData();
		}


		private function removeEvents(sf:SmartFox):void {
			sf.removeEventListener(SFSEvent.CONNECTION, onConnectionListener);
			sf.removeEventListener(SFSEvent.CONNECTION_LOST, onConnectionLostListener);
			sf.removeEventListener(SFSEvent.CONNECTION_RETRY, onConnectionRetryListener);
			sf.removeEventListener(SFSEvent.CONNECTION_RESUME, onConnectionResumeListener);


			sf.removeEventListener(SFSEvent.LOGIN, onLoginListener);
			sf.removeEventListener(SFSEvent.LOGOUT, onLogoutListener);

			sf.removeEventListener(SFSEvent.USER_COUNT_CHANGE, onUserCountChangeListener);

			sf.removeEventListener(SFSEvent.ROOM_JOIN, onRoomJoinListener);
			sf.removeEventListener(SFSEvent.ROOM_JOIN_ERROR, onRoomJoinErrorListener);
			sf.removeEventListener(SFSEvent.ROOM_ADD, onRoomAddedListener);
			sf.removeEventListener(SFSEvent.ROOM_REMOVE, onRoomRemoveListener);
			sf.removeEventListener(SFSEvent.ROOM_NAME_CHANGE, onRoomNameChangeListener);
			sf.removeEventListener(SFSEvent.ROOM_CAPACITY_CHANGE, onRoomCapacityChangedListener);
			sf.removeEventListener(SFSEvent.ROOM_PASSWORD_STATE_CHANGE, onRoomPasswordStateChangeListener);
		}


		private function onConnectionListener(evt:SFSEvent):void {
			invalidateSkinState();
			roomsListData=null;
			mytrace("onConnectionListener " + evt);
		}

		private function onConnectionLostListener(evt:SFSEvent):void {
			roomsListData=null;
			invalidateSkinState();
			mytrace("onConnectionLostListener " + evt + "params: " + ObjectUtil.toString(evt.params));
		}

		private function onConnectionRetryListener(evt:SFSEvent):void {
			roomsListData=null;
			mytrace("onConnectionRetryListener " + evt);
		}

		private function onConnectionResumeListener(evt:SFSEvent):void {
			mytrace("onConnectionResumedListener " + evt);
			invalidateSkinState();
		}

		private function onLoginListener(evt:SFSEvent):void {
			mytrace("onLoginListener " + evt);
			generateRoomListData();
			invalidateSkinState();

		}

		private function onLogoutListener(evt:SFSEvent):void {
			mytrace("onLogoutListener " + evt);
			generateRoomListData();
			invalidateSkinState();
		}

		private function onUserCountChangeListener(evt:SFSEvent):void {
			mytrace("onUserCountChangeListener " + evt);
			updateRoom((evt.params.room as SFSRoom));
		}

		private function onRoomJoinListener(evt:SFSEvent):void {
			mytrace("onRoomJoinListener " + evt);
			generateRoomListData();
			updateRoom((evt.params.room as SFSRoom));
		}


		private function onExitListener(evt:SFSEvent):void {
			mytrace("onRoomJoinListener " + evt);
			generateRoomListData();
			updateRoom((evt.params.room as SFSRoom));
		}


		private function onRoomJoinErrorListener(evt:SFSEvent):void {
			mytrace("onRoomJoinErrorListener " + evt + "params: " + ObjectUtil.toString(evt.params));

			Alert.show(evt.params.errorMessage);
			generateRoomListData();
		}

		private function onRoomAddedListener(evt:SFSEvent):void {
			mytrace("onRoomAddedListener " + evt);
			generateRoomListData();
		}

		private function onRoomRemoveListener(evt:SFSEvent):void {
			mytrace("onRoomRemovedListener " + evt);
			generateRoomListData();
		}

		private function onRoomNameChangeListener(evt:SFSEvent):void {
			mytrace("onRoomNameChangedListener " + evt);
			updateRoom((evt.params.room as SFSRoom));
		}

		private function onRoomCapacityChangedListener(evt:SFSEvent):void {
			mytrace("onRoomCapacityChangedListener " + evt);
			updateRoom((evt.params.room as SFSRoom));
		}

		private function onRoomPasswordStateChangeListener(evt:SFSEvent):void {
			mytrace("onRoomPasswordStateChangedListener " + evt);
			updateRoom((evt.params.room as SFSRoom));
		}


		private function isLoggedIn():Boolean {
			return (smartFox && smartFox.mySelf);
		}


		private function transformListItems(items:Array):IList {
			var retObj:ArrayCollection=new ArrayCollection();
			items.forEach(function(element:*, index:int, arr:Array):void {
				if (element && element is SFSRoom) {
					retObj.addItem(generateItem(SFSRoom(element)));
				}
			});
			if (retObj.length > 0) {
				initCursor(retObj);
			}
			return retObj;
		}

		private function generateItem(sfr:SFSRoom):Object {
			
		return 	{
				id: sfr.id, 
				name: sfr.name, 
				groupId: sfr.groupId, 
				isGame: sfr.isGame, 
				isHidden: sfr.isHidden, 
				isJoined: sfr.isJoined, 
				isManaged: sfr.isManaged,
				//variables:not yet,
				//properties:not yet,
				maxUsers: sfr.maxUsers, 
				maxSpectators: sfr.maxSpectators, 
				userCount: sfr.userCount, 
				specCount: sfr.spectatorCount, 
				isPasswordProtected: sfr.isPasswordProtected
			};
		}
	

		private function updateRoom(r:SFSRoom):void {
			var found:Boolean=cursor.findAny({"id": r.id});
			if (!found) {
				Alert.show("we could not find room with id " + r.id + " in our list ");
				return;
			}
			var newObj:Object=generateItem(r);
			var col:ArrayCollection=ArrayCollection(cursor.view);
			col.setItemAt(newObj, col.getItemIndex(cursor.current));
		}

		private var cursor:IViewCursor;

		private function initCursor(arrColl:ArrayCollection):void {
			var sortField:SortField=new SortField("id", true, true);
			var sort:Sort=new Sort();
			sort.fields=[sortField];
			arrColl.sort=sort;
			arrColl.refresh();
			cursor=arrColl.createCursor();
		}

		private function mytrace(s:String):void {
			trace(getQualifiedClassName(this) + " " + s);
		}
	}
}


