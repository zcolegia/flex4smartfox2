package com.fsf2.components {

	import avmplus.getQualifiedClassName;
	
	import com.smartfoxserver.v2.SmartFox;
	import com.smartfoxserver.v2.core.SFSEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import spark.components.ToggleButton;
	import spark.components.supportClasses.ButtonBase;
	import spark.components.supportClasses.SkinnableComponent;
	import spark.components.supportClasses.TextBase;
	import spark.components.supportClasses.ToggleButtonBase;

	[SkinState("disconnected")]
	[SkinState("connecting")]
	[SkinState("connected")]

	public class SF2Connector extends SkinnableComponent {
	

		[SkinPart(required="false")]
		public var connectionLabel:TextBase;

		[SkinPart(required="false")]
		public var connectButton:ButtonBase;

		[SkinPart(required="false")]
		public var disconnectButton:ButtonBase;

		[SkinPart(required="false")]
		public var connectDisconnectToggle:ToggleButtonBase;


		private var _smartFox:SmartFox;

		public function get smartFox():SmartFox {
			return _smartFox;
		}

		public function set smartFox(value:SmartFox):void {

			if (_smartFox === value)
				return;

			if (_smartFox) {
				removeEvents(_smartFox);
				if (this.isConnected) {
					disconnect();

				}
			}

			_smartFox=value;
			if (_smartFox) {
				this._isConnected=_smartFox.isConnected;
				addEvents(_smartFox);
			}
		}


		private var _autoCreate:Boolean;

		public function get autoCreate():Boolean {
			return _autoCreate;
		}

		public function set autoCreate(value:Boolean):void {
			_autoCreate=value;
		}


		private var _autoConnect:Boolean;

		public function get autoConnect():Boolean {
			return _autoConnect;
		}

		public function set autoConnect(value:Boolean):void {
			_autoConnect=value;
		}

		private var _configXML:String="sfs-config.xml";

		public function get configXML():String {
			return _configXML;
		}

		public function set configXML(value:String):void {
			_configXML=value;
		}

		public function SF2Connector() {
			super();
		}

		override protected function getCurrentSkinState():String {
			if (this.isConnected) {
				return "connected";
			} else if (this.isConnecting) {
				return "connecting";
			} else {
				return "disconnected";
			}
		}

		override protected function partAdded(partName:String, instance:Object):void {
			super.partAdded(partName, instance);

			if (instance == connectButton) {
				connectButton.addEventListener(MouseEvent.CLICK, connectClickHadler);
				updateButtons();
			} else if (instance == disconnectButton) {
				disconnectButton.addEventListener(MouseEvent.CLICK, disconnectClickHadler);
				updateButtons();
			} else if (instance == connectDisconnectToggle) {
				connectDisconnectToggle.addEventListener(MouseEvent.CLICK, conectDiconnectClickHadler);
				updateButtons();
			} else if (instance == connectionLabel) {
				updateLabels();
			}
		}



		override protected function partRemoved(partName:String, instance:Object):void {
			super.partRemoved(partName, instance);
			if (instance == connectButton) {
				connectButton.removeEventListener(MouseEvent.CLICK, connectClickHadler);
			} else if (instance == disconnectButton) {
				disconnectButton.removeEventListener(MouseEvent.CLICK, disconnectClickHadler);
			} else if (instance == connectDisconnectToggle) {
				connectDisconnectToggle.removeEventListener(MouseEvent.CLICK, conectDiconnectClickHadler);
			}
		}

		private var _isConnected:Boolean;

		[Bindable(event="connectionChange")]
		public function get isConnected():Boolean {
			return _isConnected;
		}

		private var _isConnecting:Boolean;

		[Bindable(event="connectionChange")]
		public function get isConnecting():Boolean {
			return _isConnecting;
		}

		private var _disconnecting:Boolean;

		public function get disconnecting():Boolean {
			return _disconnecting;
		}

		public function connect():void {
			if (_isConnected || _isConnecting) {
				return;
			}

			_isConnecting=true;
			
			if(_smartFox) {
			_smartFox.connect(_smartFox.config.host, _smartFox.config.port);
			}else {
				throw new Error("SMartfox Server has not been Set ");
				return;
			}
			updateButtons();
			updateLabels();
			invalidateSkinState();
			dispatchEvent(new Event("connectionChange"));
		}

		public function disconnect():void {
			if (!_isConnected || _disconnecting) {
				return;
			}
			_disconnecting=true;
			_smartFox.disconnect();
		}


		/* ############# INTERNAL ######### */

		private function updateButtons():void {

			if (connectButton)
				connectButton.enabled=!isConnected;
			if (disconnectButton)
				disconnectButton.enabled=isConnected;
			if (connectDisconnectToggle)
				connectDisconnectToggle.selected=isConnected;
		}

		private function updateLabels():void {

			
			if(!connectionLabel) return;
			if (this._isConnected) {
				connectionLabel.text="Connected";
			} else if (this._isConnecting) {
				connectionLabel.text="Connecting ...";
			} else {
				connectionLabel.text="Not Connected";
			}

		}


		override protected function commitProperties():void {

			mytrace("SF2Connector commitProperties called ");
			if (!smartFox && autoCreate) {
				smartFox=new SmartFox();
				addEvents(smartFox);

				smartFox.loadConfig(configXML, autoConnect);
				invalidateSkinState();
			}
			super.commitProperties();
		}



		private function connectClickHadler(evt:MouseEvent):void {
			connect();
		}

		private function disconnectClickHadler(evt:MouseEvent):void {
			disconnect();
		}

		private function conectDiconnectClickHadler(evt:MouseEvent):void {
			if ((evt.currentTarget as ToggleButton).selected) {
				connect();
			} else {
				disconnect();
			}
		}

		private function addEvents(sf:SmartFox):void {

			sf.addEventListener(SFSEvent.HANDSHAKE, onHandShake);
			sf.addEventListener(SFSEvent.CONNECTION, onConnection);
			sf.addEventListener(SFSEvent.CONNECTION_LOST, onConnectionLost);
			sf.addEventListener(SFSEvent.CONNECTION_RETRY, onConnectionRetry);
			sf.addEventListener(SFSEvent.CONNECTION_RESUME, onConnectionResumed);
			sf.addEventListener(SFSEvent.CONFIG_LOAD_SUCCESS, onConfigLoadSuccess);
			sf.addEventListener(SFSEvent.CONFIG_LOAD_FAILURE, onConfigLoadFailure);
			updateButtons();
			updateLabels();
			invalidateSkinState();
		}


		private function removeEvents(sf:SmartFox):void {
			sf.removeEventListener(SFSEvent.CONNECTION, onConnection);
			sf.removeEventListener(SFSEvent.CONNECTION_LOST, onConnectionLost);
			sf.removeEventListener(SFSEvent.CONFIG_LOAD_SUCCESS, onConfigLoadSuccess);
			sf.removeEventListener(SFSEvent.CONNECTION_RETRY, onConnectionRetry);
			sf.removeEventListener(SFSEvent.CONNECTION_RESUME, onConnectionResumed);
			sf.removeEventListener(SFSEvent.CONFIG_LOAD_SUCCESS, onConfigLoadSuccess);
			sf.removeEventListener(SFSEvent.CONFIG_LOAD_FAILURE, onConfigLoadFailure);
		}


		private function onHandShake(evt:SFSEvent):void {
			mytrace("shaking hands");
		}

		private function onConnection(evt:SFSEvent):void {
			_isConnecting=false;
			_disconnecting=false;
			_isConnected=evt.params.success;
			updateButtons();
			updateLabels();
			invalidateSkinState();
			dispatchEvent(new Event("connectionChange"));
		}

		private function onConnectionLost(evt:SFSEvent):void {
			_isConnecting=false;
			_isConnected=false;
			_disconnecting=false;
			invalidateSkinState();
			updateButtons();
			updateLabels();
			dispatchEvent(new Event("connectionChange"));
		}


		private function onConnectionRetry(evt:SFSEvent):void {
			_isConnecting=true;
			_isConnected=false;
			_disconnecting=false;
			invalidateSkinState();
			updateButtons();
			updateLabels();
			dispatchEvent(new Event("connectionChange"));
			connect();
		}

		private function onConnectionResumed(evt:SFSEvent):void {
			_isConnecting=false;
			_disconnecting=false;
			_isConnected=evt.params.success;
			invalidateSkinState();
			updateButtons();
			updateLabels();
			dispatchEvent(new Event("connectionChange"));
		}

		private function onConfigLoadSuccess(evt:SFSEvent):void {
			_isConnecting=false;
			_isConnected=false;
			invalidateSkinState();
			updateButtons();
			updateLabels();
			dispatchEvent(new Event("connectionChange"));
		}

		private function onConfigLoadFailure(evt:SFSEvent):void {
			throw new Error("Configuration file " + configXML + " could not be found");
		}

		private function mytrace(s:String):void {
			trace(getQualifiedClassName(this) + " " + s);
		}
	}
}

