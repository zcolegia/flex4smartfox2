package com.fsf2.components {

	import avmplus.getQualifiedClassName;
	
	import com.hurlant.crypto.hash.IHash;
	import com.hurlant.crypto.hash.MD5;
	import com.hurlant.util.Hex;
	import com.smartfoxserver.v2.SmartFox;
	import com.smartfoxserver.v2.core.SFSEvent;
	import com.smartfoxserver.v2.requests.LoginRequest;
	import com.smartfoxserver.v2.requests.LogoutRequest;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.ByteArray;
	
	import mx.controls.Alert;
	import mx.utils.ObjectUtil;
	import mx.utils.StringUtil;
	
	import spark.components.supportClasses.ButtonBase;
	import spark.components.supportClasses.SkinnableComponent;
	import spark.components.supportClasses.SkinnableTextBase;
	import spark.components.supportClasses.TextBase;

	[SkinState("notReady")]
	[SkinState("disconnected")]
	[SkinState("connected")]
	[SkinState("loginRunning")]
	[SkinState("loggedIn")]

	public class SF2Login extends SkinnableComponent {

		[SkinPart(required="false")]
		public var usernameInput:SkinnableTextBase;

		[SkinPart(required="false")]
		public var passwordInput:SkinnableTextBase;

		[SkinPart(required="false")]
		public var loginButton:ButtonBase;

		[SkinPart(required="false")]
		public var logoutButton:ButtonBase;
		
		
		public var username:String;
		public var pass:String;


		public function get smartFox():SmartFox {
			return _smartFox;
		}

		private var _smartFox:SmartFox;

		public function set smartFox(value:SmartFox):void {


			if (_smartFox === value)
				return;

			if (_smartFox) {
				removeEvents(_smartFox);
			}

			_smartFox=value;
			if (_smartFox) {
				addEvents(_smartFox);

			}

			invalidateProperties();
			invalidateSkinState();
		}


		private var _loginRunning:Boolean;

		[Bindable(event="loginStateChanged")]
		public function get loginRunning():Boolean {
			return _loginRunning;
		}


		[Bindable(event="loginStateChanged")]
		public function get loggedIn():Boolean {
			return (this.smartFox.mySelf != null);
		}

		private var _zone:String;

		public function get zone():String {
			return _zone;
		}

		public function set zone(value:String):void {
			_zone=value;

			invalidateProperties();
		}



		public function login():void {

			if (this.loggedIn || this._loginRunning) {
				return;
			}
			this._loginRunning=true;

			dispatchEvent(new Event("loginStateChanged"));

			mytrace("logging in into the zone " + this.zone);
			
			var p:String = "";
			if(passwordInput && passwordInput.text ) {
				passwordInput.text =StringUtil.trim(passwordInput.text);
				if(passwordInput.text!="") {
					p = generateMD5(passwordInput.text);
				}
			}
			
			var u:String ="";
			if(usernameInput && usernameInput.text ) {
				u = StringUtil.trim(usernameInput.text);
			}
			
			var lr:LoginRequest=new LoginRequest(u, p, this.zone);
			smartFox.send(lr);

		}

		
		private var md5:IHash;
		private function generateMD5(val:String):String{
			if(!md5) {
				md5 = new com.hurlant.crypto.hash.MD5();
			}
			return Hex.fromArray(md5.hash(Hex.toArray(Hex.fromString(val))));
		}
		public function logout():void {
			var lr:LogoutRequest=new LogoutRequest();
			smartFox.send(lr);
		}

		public function SF2Login() {
			super();
		}

		protected override function commitProperties():void {

			if (smartFox && (!this.zone || StringUtil.trim(this.zone) == "")) {
				this.zone=smartFox.config.zone;
			}

			super.commitProperties();

		}


		override protected function getCurrentSkinState():String {

			updateControls();
			if (!this.smartFox) {
				return "notReady";
			} else {

				if (!this.smartFox.isConnected) {
					return "disconnected";
				} else {

					if (this.loginRunning) {
						return "loginRunning";
					}

					if (this.loggedIn) {
						return "loggedIn";
					} else {
						return "connected";
					}
				}
			}

		}

		override protected function partAdded(partName:String, instance:Object):void {
			super.partAdded(partName, instance);

			if (instance == usernameInput) {
				usernameInput.enabled=false;
				usernameInput.text=username;
				

			} else if (instance == passwordInput) {
				passwordInput.enabled=false;
					passwordInput.text=pass

			} else if (instance == loginButton) {

				loginButton.enabled=false;
				loginButton.addEventListener(MouseEvent.CLICK, loginClickedListener);

			} else if (instance == logoutButton) {
				logoutButton.enabled=false;
				logoutButton.addEventListener(MouseEvent.CLICK, logoutClickedListener);

			}
			updateControls();
		}

		override protected function partRemoved(partName:String, instance:Object):void {
			super.partRemoved(partName, instance);

			if (instance == usernameInput) {

			} else if (instance == passwordInput) {

			} else if (instance == loginButton) {

				loginButton.removeEventListener(MouseEvent.CLICK, loginClickedListener);

			} else if (instance == logoutButton) {
				logoutButton.removeEventListener(MouseEvent.CLICK, logoutClickedListener);

			}
		}

		private function loginClickedListener(e:MouseEvent):void {
			login();
		}

		private function logoutClickedListener(e:MouseEvent):void {
			logout();
		}


		private function updateControls():void {

			this.enabled=(this.smartFox != null);
			if (!this.smartFox) {
				return;
			}

			if (usernameInput) {
				usernameInput.enabled=(this.smartFox.isConnected);
			}
			if (passwordInput) {
				passwordInput.enabled=(this.smartFox.isConnected);
			}
			if (loginButton) {
				loginButton.enabled=(this.smartFox.isConnected && !this.loggedIn && !this._loginRunning);
			}
			if (logoutButton) {
				logoutButton.enabled=(this.smartFox.isConnected && this.loggedIn);
			}
		}

		private function addEvents(sf:SmartFox):void {

			sf.addEventListener(SFSEvent.CONNECTION, onConnection);
			sf.addEventListener(SFSEvent.CONNECTION_RETRY, onConnectionRetry);
			sf.addEventListener(SFSEvent.CONNECTION_RESUME, onConnectionResumed);

			sf.addEventListener(SFSEvent.LOGIN, onLoginListener);
			sf.addEventListener(SFSEvent.LOGOUT, onLogoutListener);

			sf.addEventListener(SFSEvent.CONNECTION_LOST, onConnectionLostListener);
			sf.addEventListener(SFSEvent.LOGIN_ERROR, onLoginErrorListener);
		}

		private function removeEvents(sf:SmartFox):void {

			sf.removeEventListener(SFSEvent.LOGIN, onLoginListener);
			sf.removeEventListener(SFSEvent.LOGOUT, onLogoutListener);
			sf.removeEventListener(SFSEvent.CONNECTION_LOST, onConnectionLostListener);
			sf.removeEventListener(SFSEvent.LOGIN_ERROR, onLoginErrorListener);
		}

		private function onLoginListener(evt:SFSEvent):void {
			mytrace("onLoginListener " + evt);
			this._loginRunning=false;

			invalidateSkinState();
			dispatchEvent(new Event("loginStateChanged"));
		}

		private function onLogoutListener(evt:SFSEvent):void {
			mytrace("onLogoutListener " + evt);
			this._loginRunning=false;

			invalidateSkinState();
			dispatchEvent(new Event("loginStateChanged"));
		}


		private function onLoginErrorListener(evt:SFSEvent):void {
			this._loginRunning=false;
			mytrace("onLoginErrorListener " + evt + "params: " + ObjectUtil.toString(evt.params));
			Alert.show(ObjectUtil.toString(evt.params));
			invalidateSkinState();
			dispatchEvent(new Event("loginStateChanged"));
		}


		private function onConnectionLostListener(evt:SFSEvent):void {

			this._loginRunning=false;

			invalidateSkinState();
			dispatchEvent(new Event("loginStateChanged"));
		}


		private function onConnection(evt:SFSEvent):void {
			this._loginRunning=false;
			invalidateSkinState();
			dispatchEvent(new Event("loginStateChanged"));
		}


		private function onConnectionRetry(evt:SFSEvent):void {
			invalidateSkinState();
			dispatchEvent(new Event("loginStateChanged"));
		}

		private function onConnectionResumed(evt:SFSEvent):void {
			invalidateSkinState();
			dispatchEvent(new Event("loginStateChanged"));
		}

		private function mytrace(s:String):void {
			trace(getQualifiedClassName(this) + " " + s);
		}

	}
}

